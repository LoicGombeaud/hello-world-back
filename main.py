from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def name():
    return {"greeting": "Hello World!"}


@app.get("/{name}")
async def hello(name):
    return {"greeting": f"Hello {name}!"}
