FROM python:3.10-alpine

RUN pip install pipenv

WORKDIR /app
ADD Pipfile.lock .
RUN pipenv sync
ADD main.py .

ENV UVICORN_HOST "0.0.0.0"
ENV UVICORN_PORT "80"

CMD ["pipenv", "run", "uvicorn", "main:app"]
